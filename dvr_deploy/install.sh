sudo systemctl stop dvr_deamon.service
sudo cp /tmp/dvr_deploy/dvr_deamon.service /etc/systemd/system/
sudo systemctl daemon-reload
sudo systemctl enable dvr_deamon.service
sudo mkdir -p /media/dvr_deploy/sub_process
cp /tmp/dvr_deploy/dvr_daemon /media/dvr_deploy
cp /tmp/dvr_deploy/sub_process/cam_mapping /media/dvr_deploy/sub_process/
sudo systemctl start dvr_deamon.service
